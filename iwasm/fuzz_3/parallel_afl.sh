#!/usr/bin/env bash
# This script creates one main (-M) and $(NPROC) secondary (-S) fuzzing process for each node.
# If you want only one main in just one node and the rest as secondary you can delete line 53 for secondary nodes
# For exchanging information amongst the nodes run distribute.sh
# This script should be ran in every node (preferably in a tmux session)
# Some parts of these script have been borrowed from the work of Prof. Dr. Regehr
# Reference link to Prof. Dr. Regehr's work: https://github.com/regehr/guided-tree-search/blob/main/aflplusplus/parallel_afl.sh

AFL_POST_PROCESS_KEEP_ORIGINAL=1 

display_help() {
    echo "Usage: $0 [options] --target \"[target command]\""
    echo "Example: $0 -pr --target \"/path/to/target <flags> @@ <flags>\""
    echo "The @@ placeholder is required for AFL to insert the input file."
    echo
    echo "Options:"
    echo "  -pr, --parametric-randomness             Run with parametric randomness"
    echo "  -wpr, --without-parametric-randomness    Run without parametric randomness"
    echo "  -t, --target                             Specify the target command and its flags"
    echo "  -h, --help                               Display this help message"
    echo
    echo "Environment Variables:"
    echo "  WASMLIKE_PATH                            Set to /path/to/wasmlike.rkt path"
    echo "                                           wasmlike.rkt under /diff_testing is the one to generate codes with crc globals"
    echo
}

# Assuming the script is run under /path/to/wasmoi/fuzz
FUZZ_DIR=$(pwd)
AFL_DIR="${FUZZ_DIR}/../AFLplusplus"

TARGET_CMD=""
MODE=""

while [[ "$#" -gt 0 ]]; do
    case "$1" in
        -pr|--parametric-randomness)
            AFL_CUSTOM_MUTATOR_LIBRARY="${AFL_DIR}/custom_mutators/examples/mut_wasm_pr.so"
            OUTPUT_DIR="output"
	    unset AFL_CUSTOM_MUTATOR_ONLY
            MODE="pr"
            shift
            ;;
        -wpr|--without-parametric-randomness)
            AFL_CUSTOM_MUTATOR_LIBRARY="${AFL_DIR}/custom_mutators/examples/mut_wasm_wpr.so"
            OUTPUT_DIR="output_wpr"
	    export AFL_CUSTOM_MUTATOR_ONLY=1
            MODE="wpr"
            shift
            ;;
        -t|--target)
            TARGET_CMD="$2"
            shift 2
            ;;
        -h|--help)
            display_help
            exit 0
            ;;
        *)
            echo "Invalid option: $1. Use -h or --help for usage information."
            exit 1
            ;;
    esac
done

if [ -z "$TARGET_CMD" ]; then
    echo "Error: Target command not specified. Use --target to specify the target."
    exit 1
fi

if [ -z "$MODE" ]; then
    echo "Error: Fuzzing mode not specified. Use -pr or -wpr."
    exit 1
fi

NPROC=$(nproc)

export AFL_SHUFFLE_QUEUE=1
export AFL_DEBUG_CHILD=1
export AFL_POST_PROCESS_KEEP_ORIGINAL AFL_CUSTOM_MUTATOR_LIBRARY
export AFL_NO_WARN_INSTABILITY=1
CMD="$TARGET_CMD"  # Use the dynamically specified target command
CMDARGS='' # Additional arguments can be part of the TARGET_CMD
AFLARGS='-i ./input -o out -D -t 800 -V 324000  -P explore -a binary -G 2097152 -e wasm' # Change AFL arguments here

WASMLIKE_PATH=/users/khan22/wasmoi/code_generator/crash_testing/wasmlike.rkt afl-fuzz $AFLARGS -M f0 -- $CMD & # Main fuzzing process

export AFL_NO_UI=1
export AFL_QUIET=1

for (( i=1; i<3; i++ ))
do
    echo $i
    WASMLIKE_PATH=/users/khan22/wasmoi/code_generator/crash_testing/wasmlike.rkt afl-fuzz $AFLARGS -S f${i} -- $CMD & # Secondary fuzzing process
done

for (( i=0; i<3; i++ ))
do
    wait
done

# ---------------------------------------------- < END OF FILE > ---------------------------------------------- #
