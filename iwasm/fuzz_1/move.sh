#!/bin/bash

# Define source and target directories
SOURCE_DIR="./queue"
TARGET_DIR="./input"

# Check if source directory exists
if [ ! -d "$SOURCE_DIR" ]; then
    echo "Source directory does not exist."
    exit 1
fi

# Check if target directory exists, create if not
if [ ! -d "$TARGET_DIR" ]; then
    echo "Target directory does not exist. Creating it."
    mkdir -p "$TARGET_DIR"
fi

# Move 30 random files from source to target directory
find "$SOURCE_DIR" -type f | shuf -n 30 | xargs -I {} mv {} "$TARGET_DIR"

echo "Moved 30 random files from $SOURCE_DIR to $TARGET_DIR."

