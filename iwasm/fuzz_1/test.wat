;; This is a RANDOMLY GENERATED PROGRAM.
;; Fuzzer: wasmlike
;; Version: wasmlike 1.0 (0034dfa), xsmith 2.0.6 (de6ace1), in Racket 8.2 (vm-type chez-scheme)
;; Options: 
;; Seed: 1983475480
;; 

(module (memory $mem 1) (global $lift_1 (mut i32) (i32.const -869))
   (table 0 funcref)
   (elem (i32.const 0))
   (func
    $main
    (export "_main")
    (result i32)
    (local $i32_storage i32)
    (local $i64_storage i64)
    (local $f32_storage f32)
    (local $f64_storage f64)
    i32.const
    740
    i32.load16_s
    offset=21
    align=1
    i32.clz
    i32.const
    642
    i32.load
    offset=53
    align=2
    i32.const
    170
    i32.load16_u
    offset=84
    align=1
    i32.rotl
    i32.rotl
    i32.const
    543
    i32.load8_s
    offset=50
    align=1
    global.get
    $lift_1
    i32.add
    global.set
    $lift_1
    global.get
    $lift_1
    i32.shl))

