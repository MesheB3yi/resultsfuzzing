Command line used to find this crash:

afl-fuzz -i ./input -o out -D -t 800 -V 324000 -P explore -a binary -G 2097152 -e wasm -S f2 -- /users/khan22/wasmoi/targets/wasm-micro-runtime/product-mini/platforms/linux/build/iwasm -f _main @@

If you can't reproduce a bug outside of afl-fuzz, be sure to set the same
memory limit. The limit used for this fuzzing session was 0 B.

Need a tool to minimize test cases before investigating the crashes or sending
them to a vendor? Check out the afl-tmin that comes with the fuzzer!

Found any cool bugs in open-source tools using afl-fuzz? If yes, please post
to https://github.com/AFLplusplus/AFLplusplus/issues/286 once the issues
 are fixed :)

