#!/usr/bin/perl -w

# ---------------------------------------------------------------------------------------- #
# Script Information:                                                                      # 
# Written in Perl, inspired by the work of Prof. Dr. Regehr.                               #
# Reference: https://github.com/regehr/guided-tree-search/blob/main/aflplusplus/average.pl #
# ---------------------------------------------------------------------------------------- #


use strict;
use File::Find;


my @tests = ("test_1", "test_2", "test_3", "test_4", "test_5");
my @base_dirs_directed = map { "/users/khan22/resultsfuzzing/fuzz_test/$_/output/fuzzer1" } @tests; # Change the target here
my @base_dirs_undirected = map { "/users/khan22/resultsfuzzing/fuzz_test/$_/output_wpr/fuzzer1" } @tests; # And here


my $block_size = 600;


my ($good, $bad);
my (%count, %total_edges, %global_block_total, %global_block_count, %global_block_min, %global_block_max);

sub process_directory {
    my ($base_dirs_ref, $output_file) = @_;

   
    $good = $bad = 0;
    %count = %total_edges = %global_block_total = %global_block_count = %global_block_min = %global_block_max = ();

    sub wanted {
        if ($_ eq "plot_data") {
            my $fn = $File::Find::name;
            print "$fn\n";
            unless (-e $fn) {
                warn "File $fn does not exist. Skipping...\n";
                return;
            }
            open my $INF, "<$fn" or die "Failed to open $fn: $!";
            my $line = <$INF>;
            while ($line = <$INF>) {
                chomp $line;
                my @s = split (/,/, $line);
                if (scalar(@s) == 13) {
                    ++$good;
                    my $time = $s[0];
                    my $edges = $s[12];
                    my $block = $block_size * int($time / $block_size);
                    $global_block_count{$block} += 1;
                    $global_block_total{$block} += $edges;
                    if (!exists $global_block_min{$block} || $edges < $global_block_min{$block}) {
                        $global_block_min{$block} = $edges;
                    }
                    if (!exists $global_block_max{$block} || $edges > $global_block_max{$block}) {
                        $global_block_max{$block} = $edges;
                    }
                } else {
                    ++$bad;
                }
            }
            close $INF;
        }
    }

    for my $base_dir (@$base_dirs_ref) {
        finddepth(\&wanted, $base_dir);
    }

    
    open my $OUTF, ">$output_file" or die;
    foreach my $k (sort {$a <=> $b} keys %global_block_count) {
        my $avg = (0.0 + $global_block_total{$k}) / $global_block_count{$k};
        my $min = $global_block_min{$k};
        my $max = $global_block_max{$k};
        my $hours = $k / (60 * 60.0);
        print $OUTF "$hours $avg $min $max\n";
    }
    close $OUTF;
}


process_directory(\@base_dirs_directed, "directed_data.txt");
process_directory(\@base_dirs_undirected, "undirected_data.txt");


open my $GP, '|-', 'gnuplot';
print {$GP} <<'__GNUPLOT__';
set terminal pdf
set output "combined_plot.pdf"
set style fill transparent solid 0.55 noborder  # Shade tranparency can be adjusted here
set ylabel "Edges Found"
set xlabel "Time (hours)"
set title "Fuzzing Results: Wasmtime"
set key inside bottom right
plot "directed_data.txt" using 1:3:4 with filledcurves lt 1 lc rgb "#FFCCCC" notitle, \
     "undirected_data.txt" using 1:3:4 with filledcurves lt 1 lc rgb "#CCCCFF" notitle, \
     "directed_data.txt" using 1:2 smooth bezier with lines linewidth 2 lt 1 lc rgb "red" title 'Directed', \
     "undirected_data.txt" using 1:2 smooth bezier with lines linewidth 2 lt 1 lc rgb "blue" title 'Undirected'
__GNUPLOT__
close $GP;

# ---------------------------------------------- < END OF FILE > ---------------------------------------------- #

